
Budmonitor est une application web permettant la visualisation de temps 
de latence de donnees dans un buffer seedlink.

Une base de donnees contenant ces temps de latence pre-calcules doit
etre fournie a l'application (voir query.py).

De plus, quelques scripts utilitaires permettent de realiser un tracé
des données récentes de chaque channel. Ces scripts utilisent un accès
a un webservice datasalect.

### STRUCTURE DES FICHIERS 

```

.
├── contributors.txt
├── monitor.py		classe principale Monitor fournissant le point d'entree WSGI
├── query.py		classe secondaire fournissant les echanges de donnees (seedlink, postgres)
├── requirements.txt	liste des depedances
├── settings.py		parametres applicatifs (settings.py.dist)
├── static		ressources statiques
│   ├── bootstrap-3.3.6-dist	Gestionnaire de layout web
│   │   ├── css
│   │   │   ├── bootstrap.css
│   │   │   ├── bootstrap.css.map
│   │   │   ├── bootstrap.min.css
│   │   │   ├── bootstrap.min.css.map
│   │   │   ├── bootstrap-theme.css
│   │   │   ├── bootstrap-theme.css.map
│   │   │   ├── bootstrap-theme.min.css
│   │   │   └── bootstrap-theme.min.css.map
│   │   ├── fonts
│   │   │   ├── glyphicons-halflings-regular.eot
│   │   │   ├── glyphicons-halflings-regular.svg
│   │   │   ├── glyphicons-halflings-regular.ttf
│   │   │   ├── glyphicons-halflings-regular.woff
│   │   │   └── glyphicons-halflings-regular.woff2
│   │   └── js
│   │       ├── bootstrap.js
│   │       ├── bootstrap.min.js
│   │       └── npm.js
│   ├── img				quelques images statiques
│   │   ├── primary-plot-14px.png
│   │   ├── primary-plot-300px.png
│   │   └── resif_header.png
│   ├── resif.css			feuille de style
│   └── resif.js			interaction UI web pour affichage des plots
├── templates				templates Mako
│   ├── 404.html
│   ├── base.html
│   ├── error.html
│   ├── index.html
│   └── station.html
├── test				classe de test
│   └── test_budmonitor.py
├── misc scripts                        scripts utilitaires
├── utils.py				qq fonctions utilitaires (logger, ...)
└── wsgi.py				point d'entree Apache

```


### SCHEMA D'EXECUTION 

```


					--------------> settings.py
					|
					|
Apache --------> WSGI ----> Monitor.application  ------> obspy
					|
					|-------------> Mako
				----> parsing de self.url
					---> /index
					---> /static
					---> /plot
					---> /notfound
					---> /error

```



