# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1465910517.993268
_enable_loop = True
_template_filename = u'/var/www/budmonitor/templates/base.html'
_template_uri = u'/base.html'
_source_encoding = 'ascii'
_exports = [u'content']


def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def content():
            return render_content(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer(u'<html>\n')
        __M_writer(u'\n<head>\n    <title>RESIF - BUD Monitor</title>\n  \n    <!-- jquery -->\n\t<script src="https://code.jquery.com/jquery-2.2.4.min.js"\n\t\t\tintegrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="\n\t\t\tcrossorigin="anonymous"></script>\n\t<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"\n\t\t  \tintegrity="sha256-xNjb53/rY+WmG+4L6tTl9m6PpqknWZvRt0rO1SRnJzw="\n\t\t  \tcrossorigin="anonymous"></script>\n\t\t  \t\n\t<!-- bootstrap --> \t\n\t<!--   \t\n\t<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>\n\t<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">\t\n\t-->\n\t<script src="/budmonitor/static/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>\n\t<link rel="stylesheet" href="/budmonitor/static/bootstrap-3.3.6-dist/css/bootstrap.min.css">\n\t\n\t<!-- custom -->\n\t<script src="/budmonitor/static/resif.js"></script>\n\t<link rel="stylesheet" href="/budmonitor/static/resif.css">\n\t\n\t<!-- rendering on mobile devices -->\n\t<meta name="viewport" content="width=device-width, initial-scale=1">\n</head>\n<body>\n<div class="container">\n    <div class="row row-centered">\n        <div class="col-xs-6 col-centered col-fixed" id="header"></div>\n    </div>\n\t<div class="row row-centered">\n\t<!-- Fixed navbar -->\n\t<nav class="navbar navbar-default col-centered col-xs-6 col-fixed">\n\t\t<div class="navbar-inner">\n\t \t\t<div class="container">\n\t   \t\t\t<div class="navbar-header">\n\t     \t\t\t<a class="navbar-brand" href="/">\n\t     \t\t\t\t<span class="glyphicon glyphicon-home"></span> \n\t     \t\t\t</a>\n\t   \t\t\t</div>\n\t\t\t\t<!--\n\t   \t\t\t<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">\n\t     \t\t\t<ul class="nav navbar-nav">\n\t     \t\t\t \t<li><a href="/" class="btn btn-lg">Seismic data</a></li>\n\t     \t\t\t \t<li><a href="/" class="btn btn-lg">Seismic networks</a></li>\n\t     \t\t\t \t<li><a href="/" class="btn btn-lg">Citing data\tOrganization</a></li>\n\t     \t\t\t \t<li><a href="/" class="btn btn-lg">Support</a></li>\n\t     \t\t\t</ul>\n\t   \t\t\t</div>\n\t\t\t\t-->\n\t   \t\t</div>\n\t \t</div>\n\t</nav>\n\t</div>\n\t<div class="row row-centered">\n\t\t<div class="maintitle col-centered col-xs-6 col-fixed">Seedlink data latency</div>\n\t</div>\n\t')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer(u'\n</div><!-- Container -->\n\t\t\t\n</body>\n</html>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        __M_writer = context.writer()
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "ascii", "line_map": {"35": 61, "46": 35, "16": 2, "23": 1, "24": 2, "29": 61}, "uri": "/base.html", "filename": "/var/www/budmonitor/templates/base.html"}
__M_END_METADATA
"""
