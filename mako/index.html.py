# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1475568212.626299
_enable_loop = True
_template_filename = '/var/www/budmonitor/templates/index.html'
_template_uri = 'index.html'
_source_encoding = 'ascii'
_exports = [u'content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'/base.html', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def content():
            return render_content(context._locals(__M_locals))
        networks = context.get('networks', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer(u'\n\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        networks = context.get('networks', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n\t<div class="row row-centered">\n\t\t<div class="col-centered col-xs-6 col-fixed">\n\t\t\t<div class="panel panel-default">\n\t\t  \t\t<div class="panel-heading">\n\t\t    \t\t<h3 class="panel-title">Latency definition</h3>\n\t\t \t\t</div>\n\t\t  \t\t<div class="panel-body">\n\t\t  \t\t\t<span class="label label-def label-invalid">invalid</span>\n\t  \t\t\t\t<span class="label label-def label-inf1min">&lt; 1 min</span>\n\t  \t\t\t\t<span class="label label-def label-sup1min">&ge; 1 min</span>\n\t  \t\t\t\t<span class="label label-def label-sup10min">&gt 10 min</span>\n\t  \t\t\t\t<span class="label label-def label-sup30min">&gt 30 min</span>\n\t  \t\t\t\t<span class="label label-def label-sup1hour">&gt 1 hour</span>\n\t  \t\t\t\t<span class="label label-def label-sup2hour">&gt 2 hours</span>\n\t  \t\t\t\t<span class="label label-def label-sup6hour">&gt 6 hours</span>\n\t  \t\t\t\t<span class="label label-def label-sup1day">&gt 1 day</span>\n\t  \t\t\t\t<span class="label label-def label-sup2days">&gt 2 days</span>\n\t  \t\t\t\t<span class="label label-def label-sup3days">&gt 3 days</span>\n\t  \t\t\t\t<span class="label label-def label-sup4days">&gt 4 days</span>\n\t  \t\t\t\t<span class="label label-def label-sup5days">&gt 5 days</span>\n\t\t  \t\t</div>\n\t\t\t</div>\n')
        for network in networks:
            __M_writer(u'\t\t   \t<div class="panel panel-default">\n\t\t  \t\t<div class="panel-heading">\n\t\t    \t\t<h3 class="panel-title"><A HREF="http://seismology.resif.fr/#NetworkConsultPlace:')
            __M_writer(unicode(network['id']))
            __M_writer(u'">Network ')
            __M_writer(unicode(network['id']))
            __M_writer(u'</A></h3>\n\t\t \t\t</div>\n\t\t  \t\t<div class="panel-body">\n')
            for station in network['stations']:
                __M_writer(u'\t\t  \t\t\t\t<a href="/budmonitor/station/')
                __M_writer(unicode(network['id']))
                __M_writer(u'/')
                __M_writer(unicode(station['id']))
                __M_writer(u'">\n\t\t  \t\t\t\t\t<span class="label label-')
                __M_writer(unicode(station['label']))
                __M_writer(u'">')
                __M_writer(unicode(station['id']))
                __M_writer(u'</span>\n\t\t  \t\t\t\t</a>\n')
            __M_writer(u'\t\t  \t\t</div>\n\t\t\t</div>\n')
        __M_writer(u'\t\t    \n\t\t</div>\n    </div>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "ascii", "line_map": {"27": 0, "35": 1, "40": 43, "46": 3, "53": 3, "54": 26, "55": 27, "56": 29, "57": 29, "58": 29, "59": 29, "60": 32, "61": 33, "62": 33, "63": 33, "64": 33, "65": 33, "66": 34, "67": 34, "68": 34, "69": 34, "70": 37, "71": 40, "77": 71}, "uri": "index.html", "filename": "/var/www/budmonitor/templates/index.html"}
__M_END_METADATA
"""
