# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1465910525.695851
_enable_loop = True
_template_filename = '/var/www/budmonitor/templates/station.html'
_template_uri = 'station.html'
_source_encoding = 'ascii'
_exports = [u'content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'/base.html', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def content():
            return render_content(context._locals(__M_locals))
        channels = context.get('channels', UNDEFINED)
        station = context.get('station', UNDEFINED)
        network = context.get('network', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        channels = context.get('channels', UNDEFINED)
        station = context.get('station', UNDEFINED)
        network = context.get('network', UNDEFINED)
        __M_writer = context.writer()
        __M_writer(u'\n\t<div class="row row-centered">\n\t\t<div class="col-centered col-xs-6 col-fixed">\n\t\t\t<div class="panel panel-default">\n\t\t  \t\t<div class="panel-heading">\n\t\t    \t\t<h3 class="panel-title">Latency definition</h3>\n\t\t \t\t</div>\n\t\t  \t\t<div class="panel-body">\n\t\t  \t\t\t<span class="label label-def label-invalid">invalid</span>\n\t  \t\t\t\t<span class="label label-def label-inf1min">&lt; 1 min</span>\n\t  \t\t\t\t<span class="label label-def label-sup1min">&ge; 1 min</span>\n\t  \t\t\t\t<span class="label label-def label-sup10min">&gt 10 min</span>\n\t  \t\t\t\t<span class="label label-def label-sup30min">&gt 30 min</span>\n\t  \t\t\t\t<span class="label label-def label-sup1hour">&gt 1 hour</span>\n\t  \t\t\t\t<span class="label label-def label-sup2hour">&gt 2 hours</span>\n\t  \t\t\t\t<span class="label label-def label-sup6hour">&gt 6 hours</span>\n\t  \t\t\t\t<span class="label label-def label-sup1day">&gt 1 day</span>\n\t  \t\t\t\t<span class="label label-def label-sup2days">&gt 2 days</span>\n\t  \t\t\t\t<span class="label label-def label-sup3days">&gt 3 days</span>\n\t  \t\t\t\t<span class="label label-def label-sup4days">&gt 4 days</span>\n\t  \t\t\t\t<span class="label label-def label-sup5days">&gt 5 days</span>\n\t\t  \t\t</div>\n\t\t\t</div>\n\t\t\t\n\t\t\t<!-- Modal -->\n\t\t\t<div id="myModal" class="modal fade" role="dialog">\n\t\t\t  <div class="modal-dialog">\n\t\t\t    <!-- Modal content-->\n\t\t\t    <div class="modal-content">\n\t\t\t      <div class="modal-header">\n\t\t\t        <button type="button" class="close" data-dismiss="modal">&times;</button>\n\t\t\t        <h4 class="modal-title" id="seedplottitle"></h4>\n\t\t\t      </div>\n\t\t\t      <div class="modal-body">\n\t\t\t      \t<div class="loading">Please wait the data load.</div>\n\t\t\t\t\t<div class="loading loader"></div>\n\t\t\t\t\t<div id="seedplot"></div>        \n\t\t\t      </div>\n\t\t\t      <div class="modal-footer">\n\t\t\t        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>\n\t\t\t      </div>\n\t\t\t    </div>\n\t\t\t  </div>\n\t\t\t</div>\n\t\t\t\n\t\t\t<div class="panel panel-default">\n\t\t\t\t<div class="panel-heading">\n\t\t    \t\t<h3 class="panel-title"><a href="/budmonitor/">All networks</a> > Network : ')
        __M_writer(unicode(network))
        __M_writer(u' > Station : ')
        __M_writer(unicode(station))
        __M_writer(u'</h3>\n\t\t \t\t</div>\n\t\t \t\t<div class="panel-body">\n\t\t \t\t\t<div class="table-responsive">\n\t\t \t\t \t\t<table class="table">\n\t\t \t\t \t\t<thead>\n\t\t \t\t \t\t\t<tr>\n\t\t \t\t \t\t\t\t<th>Location</th>\n\t\t \t\t \t\t\t\t<th>Channel</th>   \n\t\t\t\t\t\t\t\t<th>Data latency</th>\n\t\t\t\t\t\t\t\t<th>Feed latency</th>\n\t\t\t\t\t\t\t\t<th>Total latency</th>\n\t\t\t\t\t\t\t\t<th>Waveform plot</th>\n\t\t \t\t \t\t\t</tr>\n\t\t \t\t \t\t</thead>\n\t\t \t\t \t\t<tbody>\n')
        for channel in channels:
            __M_writer(u'\t\t \t\t \t\t<tr>\n      \t\t\t\t\t\t<td>')
            __M_writer(unicode(channel['location']))
            __M_writer(u'</th>\n      \t\t\t\t\t\t<td>')
            __M_writer(unicode(channel['channel']))
            __M_writer(u'</td>\n      \t\t\t\t\t\t<td class="')
            __M_writer(unicode(channel['dataLatencyLabel']))
            __M_writer(u'">')
            __M_writer(unicode(channel['dataLatency']))
            __M_writer(u'</td>\n      \t\t\t\t\t\t<td class="')
            __M_writer(unicode(channel['feedLatencyLabel']))
            __M_writer(u'">')
            __M_writer(unicode(channel['feedLatency']))
            __M_writer(u'</td>\n      \t\t\t\t\t\t<td class="')
            __M_writer(unicode(channel['totalLatencyLabel']))
            __M_writer(u'">')
            __M_writer(unicode(channel['totalLatency']))
            __M_writer(u'</td>\n      \t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<button type="button" class="btn btn-plot" seedid="')
            __M_writer(unicode(channel['seedid']))
            __M_writer(u'">\n      \t\t\t\t\t\t\t\t<img src="/budmonitor/static/img/primary-plot-14px.png" height="14" width="14">\n    \t\t\t\t\t\t\t</button>\n\t\t  \t\t\t\t\t</td>\n    \t\t\t\t\t</tr>\n')
        __M_writer(u'\t\t\t  \t\t\t</tbody>\n\t\t\t  \t\t\t</table>\n\t\t\t\t\t</div> \n\t\t  \t\t</div>\n\t\t\t</div>\n\t\t</div>\n    </div>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "ascii", "line_map": {"27": 0, "37": 1, "42": 87, "48": 3, "57": 3, "58": 50, "59": 50, "60": 50, "61": 50, "62": 66, "63": 67, "64": 68, "65": 68, "66": 69, "67": 69, "68": 70, "69": 70, "70": 70, "71": 70, "72": 71, "73": 71, "74": 71, "75": 71, "76": 72, "77": 72, "78": 72, "79": 72, "80": 74, "81": 74, "82": 80, "88": 82}, "uri": "station.html", "filename": "/var/www/budmonitor/templates/station.html"}
__M_END_METADATA
"""
