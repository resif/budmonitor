#!/usr/bin/env python2.7
"""
Plot recent waveform from channels available in BUD database.
Data is taken from FDSNWS dataselect.
"""

import settings
from utils import logger
from query import QueryPostgresql,QuerySeedlink
from shutil import copyfile

# Force matplotlib to not use any Xwindows backend.
import matplotlib
matplotlib.use('Agg')

# for fdsnws HTTP request
from obspy.clients.fdsn import Client
from obspy import UTCDateTime
	
if __name__ == '__main__':
    myquery = QueryPostgresql()
    myseedlink = QuerySeedlink()   
    rows = myquery.get_all_channels_ids()
    client = Client("RESIF")

    for row in rows:
        channelcode = "{net}_{sta}:{loc} {cha}".format(net=row[0],sta=row[1],loc=row[2],cha=row[3])
        imagefile = "./tmp/{}.png".format(channelcode)         
        logger.debug("Getting waveform for {}".format(channelcode))
        t = UTCDateTime()
        try:
            stream = client.get_waveforms(row[0], row[1], row[2], row[3], t - 3600 , t - 600 )
            logger.debug("Plotting data.")
            stream.plot ( outfile = imagefile )
        except Exception as e:
            logger.debug("No data or error from {}".format(channelcode))
            copyfile("./static/noplotavailable.png", imagefile)
        

        #st = myseedlink.get_stream(channelcode, settings.SL_RANGE_TIME, settings.SL_DELTA_END_TIME)
        #status = str(st)
        #logger.debug("Seedlink query returns : {}".format(status))
        
        #if not status.startswith("0 Trace(s)"):
        #    logger.debug("plotting...")
        #    stfile = "/var/www/budmonitor/{}.png".format(channelcode)
        #    st.plot(outfile=stfile)
        #else:
        #    copyfile("/var/www/budmonitor/static/noplotavailable.png", stfile)
        
        
