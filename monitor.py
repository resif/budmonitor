import re
import os
import sys
import base64
import time
from cgi import parse_qs, escape
from mako.lookup import TemplateLookup
# project import
import settings
from query import QueryPostgresql, QuerySeedlink, BUDException
from utils import logger


latency_lookup_table = [
                 ('invalid',-sys.maxint,0),
                 ('inf1min', 0, 60),
                 ('sup1min', 60, 600),
                 ('sup10min', 600, 1800),
                 ('sup30min', 1800, 3600),
                 ('sup1hour', 3600, 7200),
                 ('sup2hour', 7200, 21600),
                 ('sup6hour', 21600, 86400),
                 ('sup1day', 86400, 172800),
                 ('sup2days', 172800, 259200),
                 ('sup3days', 259200, 345600),
                 ('sup4days', 345600, 432000),
                 ('sup5days', 432000, sys.maxsize),
                 ]

def catch_error(func):
    """ Decorator for catching all error and return a beautiful
        error page ;)
    """
    def func_wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            logger.error("Exception : {}".format(e))
        return [args[0].error_template.render().encode('utf-8')]    
    return func_wrapper
   
class Monitor:
    def __init__(self):
        """ Initilize : map urls to functions
            and template lookup directory
        """
        self.urls = [
                     (r'^$', self.index),
                     (r'station/(\w+)/(\w+)$', self.station),
#                     (r'plot$', self.plot),
                     (r'plot_static$', self.plot_static)
                     ]
        self.lookup = TemplateLookup(directories=[settings.TEMPLATE_DIR], module_directory=settings.MODULE_DIR)
        self.error_template = self.lookup.get_template('error.html')
    
    def time2label(self, timedelta):
        """ Convert timedelta in label with the latency_lookup_table
        """
        total = timedelta.total_seconds()
        label = None
        for latency in latency_lookup_table:
            if latency[1] <= total < latency[2]:
                label = latency[0]
                break
        if label is None:
            logger.error('lookup latency failed. No label found for value {}'.format(total))
            raise BUDException('Lookup latency failed.')
        return label
    
    def time2pretty(self, timedelta):
        """ Convert timedelta to human readable string
        """
        hours, remainder = divmod(timedelta.seconds, 3600)
        minutes, seconds = divmod(remainder, 60)
        timestr = "{day} days {hour:02d}:{min:02d}:{sec:02d}".format(day=timedelta.days, hour=hours, min=minutes, sec=seconds)
        return timestr
    
    @catch_error
    def index(self, environ, start_response):
        """This function will serve the / url"""
        template = self.lookup.get_template('index.html')
        start_response('200 OK', [('Content-Type', 'text/html')])
        logger.debug("index required")
        raw_networks = []
        try:
            pgquery = QueryPostgresql()
            raw_networks = pgquery.get_networks()
            logger.debug("networks results length {}".format(len(raw_networks)))
        except BUDException as e:
            logger.error("BUDException : {}".format(e))
            return [self.error_template.render().encode('utf-8')]
        pretty_networks = []
        last_network_id = None
        network = None
        for data in raw_networks:
            if data[0] != last_network_id:
                if network is not None:
                    pretty_networks.append(network)
                last_network_id = data[0]
                network = {}
                network['id'] = last_network_id
                network['stations'] = []
            network['stations'].append({'id':data[1],'label':self.time2label(data[2])})
        if network is not None:
            pretty_networks.append(network)
        return [template.render(networks=pretty_networks).encode('utf-8')]
    
    @catch_error    
    def station(self, environ, start_response):
        """ This function will serve the station details
            get the network and station id from the url.
        """
        logger.debug("station required")
        url_args = environ['bud.url_args']
        station = None
        network = None
        raw_channels = []
        if url_args:
            network = escape(url_args[0])
            station = escape(url_args[1])
        if network is None or station is None:
            return self.not_found(environ, start_response)
        try:
            pgquery = QueryPostgresql()
            raw_channels = pgquery.get_channels(network, station)
        except BUDException as e:
            logger.error("BUDException : {}".format(e))
            return [self.error_template.render().encode('utf-8')]
        if len(raw_channels) == 0:
            logger.warning("No data in the station {}".format(station))
        else:
            network = raw_channels[0][0]
            
        pretty_channels = []
        fields = ['network', 'station', 'location', 'channel', 'td', 'ta', 'tm', 'dataLatency', 'feedLatency', 'totalLatency']
        for data in raw_channels:
            channel = {'dataLatencyLabel':'', 'feedLatencyLabel':'','totalLatencyLabel':''}
            for index, field in enumerate(fields):
                channel[field] = data[index]
            for field in ['dataLatency', 'feedLatency', 'totalLatency']:
                channel[field+'Label'] = self.time2label(channel[field])
                channel[field] = self.time2pretty(channel[field])
            channel['seedid'] = "{net}_{sta}:{loc} {cha}".format(net=network,sta=station,loc=channel['location'],cha=channel['channel'])
            pretty_channels.append(channel)
            
        template = self.lookup.get_template('station.html')
        start_response('200 OK', [('Content-Type', 'text/html')])
        return [template.render(network=network, station=station, channels=pretty_channels).encode('utf-8')]

    def plot_static(self, environ, start_response):
        """
        FIXME add comment
        """
        import urllib
        logger.debug('plot_static required')
        seedid = None
        try:
            request_body_size = int(environ.get('CONTENT_LENGTH', 0))
            request_body = environ['wsgi.input'].read(request_body_size)
            params = parse_qs(request_body)
            seedid = params['seedid'][0]
        except (ValueError):
            logger.error("Get post parameters failed.")

        if seedid is not None:
            start_response('200 OK', [('Content-type', 'image/png; charset=UTF-8')])
            data = urllib.urlopen('http://webadmin-geodata.ujf-grenoble.fr/portalproducts/seedlinkplots/' + seedid + '.png').read()
            return base64.b64encode(data)

    
#    def plot(self, environ, start_response):
#        """ Call seedlink server to retriev last data
#            Convert into a beautiful plot and stream it to
#            javascript caller function.
#        """
#        
#        logger.debug('plot required')
#        
#        import matplotlib
#	    # Force matplotlib to not use any Xwindows backend.
#        matplotlib.use('Agg')

#        seedid = None
#        try:
#            request_body_size = int(environ.get('CONTENT_LENGTH', 0))
#            request_body = environ['wsgi.input'].read(request_body_size)
#            params = parse_qs(request_body)
#            seedid = params['seedid'][0]
#        except (ValueError):
#            logger.error("Get post parameters failed.")
#        if seedid is not None:
#            query = QuerySeedlink()
#            #st = query.get_stream(seedid, settings.SL_RANGE_TIME, settings.SL_DELTA_END_TIME)
#            st = query.get_stream(seedid, 30, 300)
#            status = str(st)
#            logger.debug("query seedlink return : {}".format(status))
#            if not status.startswith("0 Trace(s)"):
#                seedidpretty = seedid.replace(':','_')
#                seedidpretty = seedidpretty.replace(' ','_')
#                stfile = os.path.join(settings.SL_TMP_DIR, 'plot_'+seedidpretty+'.png')
#                #stfile = os.path.join(settings.SL_TMP_DIR, 'plot.png')
#                start = time.time()
#                logger.debug("convert stream into plot in file : {}".format(stfile))
#                st.plot(outfile=stfile)
#                end = time.time()
#                logger.debug("convert finished in {} time".format(end-start))
#                data = None
#                with open(stfile, 'rb') as image_file:
#                    data = image_file.read()
#                    start_response('200 OK', [('Content-type', 'image/png; charset=UTF-8')])
#                return base64.b64encode(data)
#            else:
#                logger.error("No data in stream")
#        logger.error("plot request failed. Return error 500")
#        start_response('500 INTERNAL SERVER ERROR', [('Content-Type', 'text/plain')])
#        return ''
            
    
    def not_found(self,environ, start_response):
        """ Called if no URL matches. """
        template = self.lookup.get_template('404.html')
        start_response('404 NOT FOUND', [('Content-Type', 'text/html')])
        return [template.render().encode('utf-8')]
    
    def application(self, environ, start_response):
        """ The main WSGI application. Dispatch the current request to
            correct methods. Store the regular expression captures in 
            the WSGI environment as `bud.url_args`.
            If nothing matches call the `not_found` function.
        """
        path = environ.get('PATH_INFO', '').lstrip('/')
        for regex, callback in self.urls:
            match = re.search(regex, path)
            if match is not None:
                environ['bud.url_args'] = match.groups()
                return callback(environ, start_response)
        return self.not_found(environ, start_response)


if __name__ == '__main__':
    """
    For local testing, install wsgiref
    and go to http://8080
    """
    from wsgiref.simple_server import make_server
    app_monitor = Monitor()
    srv = make_server('localhost', 8080, app_monitor.application)
    srv.serve_forever()
    
