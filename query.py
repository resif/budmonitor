#!/home/ludovic/.local/virtualenvs/RESIF/bin/python
# coding: utf-8
import re
import psycopg2
from utils import logger
import settings
from obspy import UTCDateTime
from obspy.clients.seedlink.basic_client import Client

class BUDException(Exception):
    def __init__(self, message):
        self.message = message
        
    def __str__(self):
        return self.message


class QuerySeedlink:
    def __init__(self, server = settings.SL_SERVER):
        self.server = server
    
    def seedid2fields(self, seedid):
        """ A seedid is a string like FR_OG02:00 HNZ
            with network_station:location channel
        """
        try:
            pattern = re.compile("^(?P<net>.+)_(?P<sta>.+):(?P<loc>.+) (?P<cha>.+)$")
            result = pattern.match(seedid)
            values = (result.group('net'), result.group('sta'), result.group('loc'), result.group('cha'))
        except Exception as e:
            logger.error("Exception : {}".format(e))
            logger.error("Convert seedid {} to fields failed".format(seedid))
            raise BUDException("seedid2fields failed.")
        return values
    
    def get_stream(self, seedid, delta, delta_end_time):
        """ Get stream from a default seedlink server for one seedid
        """
        (network, station, location, channel) = self.seedid2fields(seedid)
        logger.debug("get stream for {} with delta {} and delta_end {}".format(seedid, delta, delta_end_time))
        try:
            client = Client(self.server)
            tEnd = UTCDateTime()
            if delta_end_time is not None:
                tEnd = tEnd - int(delta_end_time)
            tStart = tEnd - int(delta)
            logger.debug("get stream from {} net {} sta {} loc {} cha {}".format(self.server,network,station,location,channel))
            logger.debug("with windows time [{} {}]".format(tStart,tEnd))
            st = client.get_waveforms(network, station, location, channel, tStart, tEnd)
        except Exception as e:
            logger.error("Exception : {}".format(e))
            logger.error("QuerySeedlink failed to get stream for seedid {}".format(seedid))
            raise BUDException("Failed to get stream.")
        return st

class QueryPostgresql:
    def __init__(self, dbname=settings.DB_NAME, 
                 user=settings.DB_USER, 
                 password=settings.DB_PASSWORD,
                 host=settings.DB_HOST):
        """ Initilize connexion to database
            raise BUDException in case of error
        """
        self.dbname = dbname
        self.user = user
        self.password = password
        self.host = host
        try:
            if self.password is None:
                self.conn = psycopg2.connect("dbname=%s user=%s host=%s" % (dbname, user, host))
            else:
                self.conn = psycopg2.connect("dbname=%s user=%s password=%s host=%s" % (dbname, user, password, host))
        except Exception as e:
            logger.error("Exception {}".format(e))
            raise BUDException("Failed to connect to database %s with user %s" % (dbname, user))
            
    def get_networks(self):
        """ get all network and associated stations
        """
        cur = None
        try:
            cur = self.conn.cursor()
            cur.execute(
                    """select distinct network, station, "totalLatency" 
                    from "LASTStationlatency" order by network;""")
        except Exception as e:
            logger.error("Exception {}".format(e))
            raise BUDException("Failed to execute get_networks query")
        return cur.fetchall()
    
    def get_channels(self, network, station):
        """ get all stations and associated location, channel
        """
        cur = None
        try:
            cur = self.conn.cursor()
            cur.execute(
                    """select network, station, location, channel, "TD", "TA", "TM",
                    "dataLatency", "feedLatency", "totalLatency" 
                    from "LASTlatency" where network = %(network)s and station = %(station)s order by location;""", {'network':network, 'station':station})
        except Exception as e:
            logger.error("Exception {}".format(e))
            raise BUDException("Failed to execute get_channels query")
        return cur.fetchall()
    
    def get_all_channels_ids(self):
        """ get all channels
        """
        cur = None
        try:
            cur = self.conn.cursor()
            cur.execute(
                    """select distinct network, station, location, channel 
                    from "LASTlatency" order by network, location;""")
        except Exception as e:
            logger.error("Exception {}".format(e))
            raise BUDException("Failed to execute get_all_channels_ids query")
        return cur.fetchall()
            

if __name__ == '__main__':
    """ For local testing launch all the methods
    """
    myquery = QueryPostgresql()
    rows = myquery.get_networks()
    for row in rows:
        print(row)
    
    rows = myquery.get_channels('FR','AJAC')
    for row in rows:
        print(row)
