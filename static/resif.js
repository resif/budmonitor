$(document).ready(function(){
	
	function drawPlot(seedid){
		jQuery.ajax({
			url	: '/budmonitor/plot_static',
			type	: 'POST',
			data	: { 'seedid':seedid },
			success : function(data){
            	console.log("success to retrieve data");
            	$('.loading').hide();
            	$('#seedplot').html('<img src="data:image/png;base64,' + data + '" />');
            },
            error : function(data){
            	console.log("error on request plot");
            }
		});
	}
	
	$(".btn-plot").click(function(){
		var seedid = this.getAttribute('seedid');
		$('#seedplot').html("");
		$('#seedplottitle').html("Latest waveform for : "+seedid);
		$('.loading').show();
		$('#myModal').modal('show');
		drawPlot(seedid);
	});
	
});
