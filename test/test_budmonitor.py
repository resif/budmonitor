"""
The bud monitor test suite.
"""
import unittest
from monitor import Monitor
from query import QuerySeedlink
from obspy import UTCDateTime
import matplotlib
import time

matplotlib.use('Agg')

class MonitorTestCase(unittest.TestCase):

    def test_seedid2fields(self):
        q = QuerySeedlink()
        fields = q.seedid2fields("FR_OG02:00 HNZ")
        self.assertEqual(fields[0], 'FR')
        self.assertEqual(fields[1], 'OG02')
        self.assertEqual(fields[2], '00')
        self.assertEqual(fields[3], 'HNZ')
    
    def test_retrieve_data(self):
        q = QuerySeedlink('rtserver.ipgp.fr')
        st = q.get_stream("G_FDF:00 BHZ",3)
        print(st)
        
    def test_generate_plot(self):
        start = time.time()
        q = QuerySeedlink('rtserve.resif.fr')
        #tEnd = UTCDateTime()
	    st = q.get_stream("G_DRV:00 BHE",30,300)
        # st = q.get_stream("FR_CHIF:00 BHN",30,300)
        # st = q.get_stream("CL_AGRP:00 BNE",30,300)
        # st = q.get_stream("G_ATD:00 BHE",30,300)
        print(st)
        st.plot(outfile="/var/www/budmonitor/tmp/out.png")
        end = time.time()
        print(end - start)
        
if __name__ == '__main__':
    """
    suite = unittest.TestLoader().loadTestsFromTestCase(MonitorTestCase)
    unittest.TextTestRunner(verbosity=2).run(suite)
    """
    unittest.main()
