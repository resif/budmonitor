import os
import sys
import site
#import matplotlib

# Force matplotlib to not use any Xwindows backend.
#matplotlib.use('Agg')

# note : Python virtualenv is off for production, but may be used later.

# Add the site-packages of the chosen virtualenv to work with
# site.addsitedir('/home/ludovic/.local/virtualenvs/RESIF/lib/python2.7/site-packages')

# Add the app's directory to the PYTHONPATH
sys.path.append('/var/www/budmonitor/')

# Activate your virtual env
# activate_env=os.path.expanduser("/home/ludovic/.local/virtualenvs/RESIF/bin/activate_this.py")
# execfile(activate_env, dict(__file__=activate_env))

from monitor import Monitor
app_monitor = Monitor()
application = app_monitor.application
